include CommonDefs.mak

BIN_DIR = Bin

INC_DIRS = include

SRC_FILES = src/main.cpp src/Skeleton.cpp src/SampleViewer.cpp src/Point3D.cpp src/SkeletonPoints.cpp src/DrawAux.cpp

ifeq ("$(OSTYPE)","Sierra")
	CFLAGS += -DMACOS
	LDFLAGS += -framework -framework
else
	CFLAGS += -DUNIX -DGLX_GLXEXT_LEGACY
	USED_LIBS +=
endif

# w przypadku kamer wykorzystujacych glebie
#CFLAGS += -DDEPTH

EXE_NAME = BodySkeletonTracker

#opencv - kompilator, biblioteki
CFLAGS += $(shell pkg-config --cflags opencv) -g -std=c++11 -pthread 
LDFLAGS += $(shell pkg-config --libs --static opencv)

INC_DIRS += $(OPENNI2_INCLUDE)

include CommonCppMakefile
