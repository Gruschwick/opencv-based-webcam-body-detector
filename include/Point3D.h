#ifndef POINT3D_H
#define POINT3D_H

#include <opencv2/highgui.hpp>

/*
*Klasa point 3D bazujaca na klasie point 2d openCV

 */
class Point3D : public cv::Point {
	public:
		Point3D(int x, int y, int z);
		Point3D(int x, int y);
		Point3D();
		virtual ~Point3D();
		
		int z;
};


#endif
