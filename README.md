# Webcam body detector based on OpenCV

Projekt realizowany na zajęcia laboratoryjne Architektury i administracji systemów operacyjnych 
oraz Obiektowych języków programowania II.

1.  Opis

    Projekt jest sterownikiem do kamerki internetowej (webcam) umożliwiając jej detekcję oraz śledzienie górnych
części ciała ludzkiego w czasie rzeczywistym. Całość bazuje na importowanych bibliotekach oprogramowania
Open CV (https://opencv.org). 

    Idea działania alogrytmu jest następująca: przetwarzając obraz rejestowany 
przez kamerkę internetową na pole bitowe, wyszukuje on punkty najbardziej skrajne (wysunięte na prawo, lewo, w górę
i w dół). Toteż do rejestrowanych punktów należą stawy ramion oraz głowa. Wyliczając medianę między punktami oraz 
nakreślając linie je łączące otrzymujemy "szkielet" nagrywanej osoby.

    Ze wględu na wykorzystywaną przeze mnie zwykłą webcam, nie jest stosowania metoda pola głębi. Do prawidłowej
detekcji w takim przypadku potrzebne jest jasne (białe), gładkie tło (patrz folder sample).

2.  Klasy

Do najważniejszych klas wykorzystywanych w ramach projektu należą:

* Point3D -  bazująca na klasie Point z OpenCV
* DrawAux - nakreślenie linii pomiędzy punktami
* Skeleton  - proces wychwytujący punkty szkieletu na podstawie miejsc zainteresowania
* SkeletonPoints - przechowująca informacje o częściach ciała
* SampleViewer - detekcja w czasie rzeczywistym

3.  Wymagania

* oporgramowanie OpenCV (wersja 3.2 wzwyż): (https://opencv.org/releases/)
* system Linux Ubuntu (realizowano w wersji 18.04)


4.  Instalacja

*  instalacja pakietu OpenCV (https://docs.opencv.org/master/d7/d9f/tutorial_linux_install.html)
*  skopiowanie pliku usr/local/lib/pkgconfig do katalogu usr/lib/pkgconfig
*  dopisanie do ukrytego w folderze home pliku .bashrc linijek
    PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/lib/pkgconfig
    export PKG_CONFIG_PATH (konfiguracja ścieżki dostępu do bibliotek OpenCV)
* pobranie katalogu projektu
* cd body -> sudo make
* wpisanie w konsoli sudo /sbin/ldconfig -v (wczytanie dostępnych bibliotek)
* cd Bin/x64-Release
* ./body 
