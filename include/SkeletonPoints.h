#ifndef SKELETON_POINTS_H
#define SKELETON_POINTS_H

#include <Point3D.h>

#define BUF_SIZE 3
#define MAX_BODY_POINTS 8

/**
 * klasa przechowyjaca informacje o czesciach ciala ze wzgledu na punkty 3D.
 * 
 **/
class SkeletonPoints {

	public:
		SkeletonPoints();
		virtual ~SkeletonPoints();
		void computePoint(int type);
		static void quick_sort(int *a, int left, int right);

		// glowne czesci ciala
		Point3D rightHand, rightElbow, rightShoulder;
		Point3D leftHand,  leftElbow,  leftShoulder;
		Point3D head;
		Point3D center;

		//wskaznik do czesci ciala
		Point3D * bodyPoints[MAX_BODY_POINTS];

		// sparametryzowanie czesci ciala jako const
		static const int HEAD          = 0;
		static const int RIGHT_HAND    = 1;
		static const int RIGHT_ELBOW   = 2;
		static const int RIGHT_SHOULDER= 3;
    		static const int LEFT_HAND     = 4;
    		static const int LEFT_ELBOW    = 5;
    		static const int LEFT_SHOULDER = 6;
		static const int CENTER        = 7;


		// Wektory czesci ciala (historia)
		Point3D pointsV[MAX_BODY_POINTS][BUF_SIZE];

		//czesci ciala glowa (last elemment added)
		unsigned char vHead[MAX_BODY_POINTS];
	private:
		void init();
		int addToVector(int type, Point3D *el);
		Point3D getMeanVector(int type);
		Point3D getMedianaVector(int type);



};

#endif
